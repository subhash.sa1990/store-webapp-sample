FROM tomcat:latest
WORKDIR /usr/share/local
ADD /target/*.jar /usr/share/local/*.jar
CMD ["catalina.sh", "run"]
